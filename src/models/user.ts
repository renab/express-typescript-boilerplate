import mongoose = require("mongoose");
import { Document, Model } from "mongoose";
import { IUser as UserInterface } from "../interfaces/user";
import { userSchema } from "../schemas/user";

export interface UserModel extends UserInterface, Document {}

export const UserModelStatic = mongoose.model<UserModel>("User", userSchema);

//export const User = mongoose.model<UserModel, UserModelStatic>("User", userSchema);