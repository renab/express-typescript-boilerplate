"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var express = require("express");
var logger = require("morgan");
var path = require("path");
var cors = require("cors");
var errorHandler = require("errorhandler");
var methodOverride = require("method-override");
var mongoose = require("mongoose");
// Routes
var index_1 = require("./routes/index");
// Schemas
var user_1 = require("./schemas/user");
// REST APIs
var user_2 = require("./routes/api/user");
/**
 * The server.
 *
 * @class Server
 */
var Server = (function () {
    /**
     * Constructor.
     *
     * @class Server
     * @constructor
     */
    function Server() {
        this.model = { user: null };
        //create expressjs application
        this.app = express();
        this.router = express.Router();
        this.app.use('/', this.router);
        //configure application
        this.config();
        //add routes
        this.routes(this.router);
        //add api
        this.api(this.router);
    }
    /**
     * Bootstrap the application.
     *
     * @class Server
     * @method bootstrap
     * @static
     * @return {ng.auto.IInjectorService} Returns the newly created injector for this app.
     */
    Server.bootstrap = function () {
        return new Server();
    };
    /**
     * Create REST API routes
     *
     * @class Server
     * @method api
     */
    Server.prototype.api = function (router) {
        var apiRouter = express.Router();
        var corsOptions = {
            allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'X-Access-Token'],
            credentials: true,
            methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
            origin: 'http://localhost:4200',
            preflightContinue: false
        };
        apiRouter.use(cors(corsOptions));
        apiRouter.get('/', function (req, res, next) {
            res.json({ announcement: 'Welcome to the API.' });
            next();
        });
        user_2.UserRoute.create(apiRouter, this.model);
        apiRouter.options('*', cors(corsOptions));
        router.use('/api', apiRouter);
    };
    /**
     * Configure application
     *
     * @class Server
     * @method config
     */
    Server.prototype.config = function () {
        var MONGODB_CONNECTION = 'mongodb://localhost:27017/db';
        //add static paths
        this.app.use(express.static(path.join(__dirname, 'public')));
        //configure pug
        this.app.set('views', path.join(__dirname, 'views'));
        this.app.set('view engine', 'pug');
        //use logger middlware
        this.app.use(logger('dev'));
        //use json form parser middlware
        this.app.use(bodyParser.json());
        //use query string parser middlware
        this.app.use(bodyParser.urlencoded({
            extended: true
        }));
        //use cookie parser middleware
        this.app.use(cookieParser('SECRET_GOES_HERE'));
        //use override middlware
        this.app.use(methodOverride());
        // Use q promises
        global.Promise = require('q').Promise;
        mongoose.Promise = global.Promise;
        // connect to mongoose
        var connection = mongoose.createConnection(MONGODB_CONNECTION);
        // create models
        this.model.user = connection.model("User", user_1.userSchema);
        //catch 404 and forward to error handler
        this.app.use(function (err, req, res, next) {
            err.status = 404;
            next(err);
        });
        //error handling
        this.app.use(errorHandler());
    };
    /**
     * Create router
     *
     * @class Server
     * @method api
     */
    Server.prototype.routes = function (router) {
        index_1.IndexRoute.create(router);
    };
    return Server;
}());
exports.Server = Server;
