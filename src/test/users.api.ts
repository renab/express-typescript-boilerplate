process.env.NODE_ENV = "test";

// mocha
import "mocha";
import { suite, test } from "mocha-typescript";

// server
import { Server } from "../server";

// model
import { IUser } from "../interfaces/user";
import { UserModel } from "../models/user";
import { userSchema } from "../schemas/user";

// mongoose
import mongoose = require("mongoose");

//require http server
var http = require("http");

//require chai and use should assertions
let chai = require("chai");
chai.should();

//configure chai-http
chai.use(require("chai-http"));

@suite class UsersAPITest {

    // constants
    public static BASE_URI: string = "/api/users";

    // the mongooose connection
    public static connection: mongoose.Connection;

    public static User: mongoose.Model<UserModel>;

    // user document
    public static user: UserModel;

    // the http server
    public static server: any;

    /**
     * Before all hook.
     */
    public static before() {
        // connect to MongoDB
        const MONGODB_CONNECTION: string = "mongodb://localhost:27017/db";
        let connection: mongoose.Connection = mongoose.createConnection(MONGODB_CONNECTION);
        UsersAPITest.User = connection.model<UserModel>("User", userSchema);

        // create http server
        let port = 8001;
        let app = Server.bootstrap().app;
        app.set("port", port);
        UsersAPITest.server = http.createServer(app);
        UsersAPITest.server.listen(port);

        return UsersAPITest.User.remove({}).then(() => {
            return UsersAPITest.createUser();
        });
    }

    /**
     * After all hook
     */
    public static after() {
        return UsersAPITest.user.remove()
            .then(() => {
                return mongoose.disconnect();
            }).catch(() => {
                return mongoose.disconnect();
            });
    }

    /**
     * Create a test user.
     */
    public static createUser(): Promise<void> {
        const data: IUser = {
            email: "foo@bar.com",
            firstName: "Joshua",
            lastName: "Faucher"
        };
        return new UsersAPITest.User(data).save().then(user => {
            UsersAPITest.user = user;
        });
    }

    @test public get() {
        return chai.request(UsersAPITest.server).get(`${UsersAPITest.BASE_URI}/${UsersAPITest.user._id}`).then(response => {
            response.should.have.status(200);
            response.body.should.be.a("object");
            response.body.should.have.property("email").eql(UsersAPITest.user.email);
            response.body.should.have.property("firstName").eql(UsersAPITest.user.firstName);
            response.body.should.have.property("lastName").eql(UsersAPITest.user.lastName);
        });
    }

    @test public list() {
        return chai.request(UsersAPITest.server).get(UsersAPITest.BASE_URI).then(response => {
            response.should.have.status(200);
            response.body.should.be.an("array");
            response.body.should.have.lengthOf(1);
        });
    }

    @test public post() {
        const data: IUser = {
            email: "foo@bar.com",
            firstName: "Joshua",
            lastName: "Faucher"
        };
        return chai.request(UsersAPITest.server).post(UsersAPITest.BASE_URI)
            .send(data)
            .then(response => {
                response.should.have.status(200);
                response.body.should.be.a("object");
                response.body.should.have.a.property("_id");
                response.body.should.have.property("email").eql(data.email);
                response.body.should.have.property("firstName").eql(data.firstName);
                response.body.should.have.property("lastName").eql(data.lastName);
                chai.request(UsersAPITest.server).del(`${UsersAPITest.BASE_URI}/${response.body._id}`).then(response => {
                    response.should.have.status(200);
                });
            });
    }

    @test public put() {
        const data: IUser = {
            email: "foo@bar.com",
            firstName: "Joshua",
            lastName: "Faucher"
        };
        return chai.request(UsersAPITest.server).put(`${UsersAPITest.BASE_URI}/${UsersAPITest.user._id}`)
            .send(data)
            .then(response => {
                response.should.have.status(200);
                response.body.should.be.a("object");
                response.body.should.have.a.property("_id");
                response.body.should.have.property("email").eql(data.email);
                response.body.should.have.property("firstName").eql(data.firstName);
                response.body.should.have.property("lastName").eql(data.lastName);
            });
    }

    @test public delete() {
        const data: IUser = {
            email: "foo@bar.com",
            firstName: "Joshua",
            lastName: "Faucher"
        };
        return chai.request(UsersAPITest.server).del(`${UsersAPITest.BASE_URI}/${UsersAPITest.user._id}`).then(response => {
            response.should.have.status(200);
        });
    }

}