"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
process.env.NODE_ENV = "test";
// mocha
require("mocha");
var mocha_typescript_1 = require("mocha-typescript");
// server
var server_1 = require("../server");
var user_1 = require("../schemas/user");
// mongoose
var mongoose = require("mongoose");
//require http server
var http = require("http");
//require chai and use should assertions
var chai = require("chai");
chai.should();
//configure chai-http
chai.use(require("chai-http"));
var UsersAPITest = (function () {
    function UsersAPITest() {
    }
    UsersAPITest_1 = UsersAPITest;
    /**
     * Before all hook.
     */
    UsersAPITest.before = function () {
        // connect to MongoDB
        var MONGODB_CONNECTION = "mongodb://localhost:27017/db";
        var connection = mongoose.createConnection(MONGODB_CONNECTION);
        UsersAPITest_1.User = connection.model("User", user_1.userSchema);
        // create http server
        var port = 8001;
        var app = server_1.Server.bootstrap().app;
        app.set("port", port);
        UsersAPITest_1.server = http.createServer(app);
        UsersAPITest_1.server.listen(port);
        return UsersAPITest_1.createUser();
    };
    /**
     * After all hook
     */
    UsersAPITest.after = function () {
        return UsersAPITest_1.user.remove()
            .then(function () {
            return mongoose.disconnect();
        });
    };
    /**
     * Create a test user.
     */
    UsersAPITest.createUser = function () {
        var data = {
            email: "foo@bar.com",
            firstName: "Joshua",
            lastName: "Faucher"
        };
        return new UsersAPITest_1.User(data).save().then(function (user) {
            UsersAPITest_1.user = user;
            return user;
        });
    };
    UsersAPITest.prototype.delete = function () {
        var data = {
            email: "foo@bar.com",
            firstName: "Joshua",
            lastName: "Faucher"
        };
        return new UsersAPITest_1.User(data).save().then(function (user) {
            return chai.request('http://localhost:8001').del(UsersAPITest_1.BASE_URI + "/" + user._id).then(function (response) {
                response.should.have.status(200);
            });
        });
    };
    UsersAPITest.prototype.get = function () {
        return chai.request('http://localhost:8001').get(UsersAPITest_1.BASE_URI + "/" + UsersAPITest_1.user._id).then(function (response) {
            response.should.have.status(200);
            response.body.should.be.a("object");
            response.body.should.have.property("email").eql(UsersAPITest_1.user.email);
            response.body.should.have.property("firstName").eql(UsersAPITest_1.user.firstName);
            response.body.should.have.property("lastName").eql(UsersAPITest_1.user.lastName);
        });
    };
    UsersAPITest.prototype.list = function () {
        return chai.request('http://localhost:8001').get(UsersAPITest_1.BASE_URI).then(function (response) {
            response.should.have.status(200);
            response.body.should.be.an("array");
            response.body.should.have.lengthOf(1);
        });
    };
    UsersAPITest.prototype.post = function () {
        var data = {
            email: "foo@bar.com",
            firstName: "Joshua",
            lastName: "Faucher"
        };
        return chai.request('http://localhost:8001').post(UsersAPITest_1.BASE_URI)
            .send(data)
            .then(function (response) {
            response.should.have.status(200);
            response.body.should.be.a("object");
            response.body.should.have.a.property("_id");
            response.body.should.have.property("email").eql(data.email);
            response.body.should.have.property("firstName").eql(data.firstName);
            response.body.should.have.property("lastName").eql(data.lastName);
            return UsersAPITest_1.User.findByIdAndRemove(response.body._id).exec();
        });
    };
    UsersAPITest.prototype.put = function () {
        var data = {
            email: "foo@bar.com",
            firstName: "Joshua",
            lastName: "Faucher"
        };
        return chai.request('http://localhost:8001').put(UsersAPITest_1.BASE_URI + "/" + UsersAPITest_1.user._id)
            .send(data)
            .then(function (response) {
            response.should.have.status(200);
            response.body.should.be.a("object");
            response.body.should.have.a.property("_id");
            response.body.should.have.property("email").eql(data.email);
            response.body.should.have.property("firstName").eql(data.firstName);
            response.body.should.have.property("lastName").eql(data.lastName);
        });
    };
    // constants
    UsersAPITest.BASE_URI = "/api/users";
    __decorate([
        mocha_typescript_1.test
    ], UsersAPITest.prototype, "delete", null);
    __decorate([
        mocha_typescript_1.test
    ], UsersAPITest.prototype, "get", null);
    __decorate([
        mocha_typescript_1.test
    ], UsersAPITest.prototype, "list", null);
    __decorate([
        mocha_typescript_1.test
    ], UsersAPITest.prototype, "post", null);
    __decorate([
        mocha_typescript_1.test
    ], UsersAPITest.prototype, "put", null);
    UsersAPITest = UsersAPITest_1 = __decorate([
        mocha_typescript_1.suite
    ], UsersAPITest);
    return UsersAPITest;
    var UsersAPITest_1;
}());
