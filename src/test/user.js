"use strict";
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
Object.defineProperty(exports, "__esModule", { value: true });
var mocha_typescript_1 = require("mocha-typescript");
var user_1 = require("../schemas/user");
var mongoose = require("mongoose");
var UserTest = (function () {
    function UserTest() {
        this.data = {
            email: "foo@bar.com",
            firstName: "Joshua",
            lastName: "Faucher"
        };
    }
    UserTest_1 = UserTest;
    UserTest.before = function () {
        //use q promises
        global.Promise = require("q").Promise;
        //use q library for mongoose promise
        mongoose.Promise = global.Promise;
        //connect to mongoose and create model
        var MONGODB_CONNECTION = "mongodb://localhost:27017/db";
        var connection = mongoose.createConnection(MONGODB_CONNECTION);
        UserTest_1.User = connection.model("User", user_1.userSchema);
        //require chai and use should() assertions
        var chai = require("chai");
        chai.should();
    };
    UserTest.prototype.create = function () {
        var _this = this;
        //create user and return promise
        return new UserTest_1.User(this.data).save().then(function (result) {
            //verify _id property exists
            result._id.should.exist;
            //verify email
            result.email.should.equal(_this.data.email);
            //verify firstName
            result.firstName.should.equal(_this.data.firstName);
            //verify lastName
            result.lastName.should.equal(_this.data.lastName);
        });
    };
    __decorate([
        mocha_typescript_1.test("should create a new User")
    ], UserTest.prototype, "create", null);
    UserTest = UserTest_1 = __decorate([
        mocha_typescript_1.suite
    ], UserTest);
    return UserTest;
    var UserTest_1;
}());
