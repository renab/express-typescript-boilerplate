// express
import { NextFunction, Response, Request, Router } from "express";
import { UserModel } from "../models/user";
import * as mongoose from "mongoose";
import {userSchema} from "../schemas/user";

/**
 * @class UsersApi
 */
export class UsersApi {

    public static User: mongoose.Model<UserModel>;

    constructor(connection: mongoose.Connection) {
        UsersApi.User = connection.model<UserModel>("User", userSchema);
    }
    
    /**
     * Create a new user.
     * @param req {Request} The express request object.
     * @param res {Response} The express response object.
     * @param next {NextFunction} The next function to continue.
     */
    public create(req: Request, res: Response, next: NextFunction) {
        // create user
        const user = new UsersApi.User(req.body);
        user.save().then(user => {
            res.json(user.toObject());
            next();
        }).catch(next);
    }

    /**
     * Delete a user.
     * @param req {Request} The express request object.
     * @param res {Response} The express response object.
     * @param next {NextFunction} The next function to continue.
     */
    public delete(req: Request, res: Response, next: NextFunction) {
        // verify the id parameter exists
        const PARAM_ID: string = "id";
        if (req.params[PARAM_ID] === undefined) {
            res.sendStatus(404);
            next();
            return;
        }

        // get id
        const id: string = req.params[PARAM_ID];

        // get user
        UsersApi.User.findById(id).then(user => {

            // verify user exists
            if (user === null) {
                res.sendStatus(404);
                next();
                return;
            }

            user.remove().then(() => {
                res.sendStatus(200);
                next();
            }).catch(next);
        }).catch(next);
    }

    /**
     * Get a user.
     * @param req {Request} The express request object.
     * @param res {Response} The express response object.
     * @param next {NextFunction} The next function to continue.
     */
    public get(req: Request, res: Response, next: NextFunction) {
        // verify the id parameter exists
        const PARAM_ID: string = "id";
        if (req.params[PARAM_ID] === undefined) {
            res.sendStatus(404);
            next();
            return;
        }

        // get id
        const id: string = req.params[PARAM_ID];

        // get user
        UsersApi.User.findById(id).then(user => {

            // verify user was found
            if (user === null) {
                res.sendStatus(404);
                next();
                return;
            }

            // send json of user object
            res.json(user.toObject());
            next();
        }).catch(next);
    }

    /**
     * List all users.
     * @param req {Request} The express request object.
     * @param res {Response} The express response object.
     * @param next {NextFunction} The next function to continue.
     */
    public list(req: Request, res: Response, next: NextFunction) {
        // get users
        UsersApi.User.find().then(users => {
            res.json(users.map(user => user.toObject()));
            next();
        }).catch(() => {
            next();
        });
    }

    /**
     * Update a user.
     * @param req {Request} The express request object.
     * @param res {Response} The express response object.
     * @param next {NextFunction} The next function to continue.
     */
    public update(req: Request, res: Response, next: NextFunction) {
        const PARAM_ID: string = "id";

        // verify the id parameter exists
        if (req.params[PARAM_ID] === undefined) {
            res.sendStatus(404);
            next();
            return;
        }

        // get id
        const id: string = req.params[PARAM_ID];

        // get user
        UsersApi.User.findById(id).then(user => {

            // verify user was found
            if (user === null) {
                res.sendStatus(404);
                next();
                return;
            }

            // save user
            Object.assign(user, req.body).save().then((user) => {
                res.json(user.toObject());
                next();
            }).catch(next);
        }).catch(next);
    }
}