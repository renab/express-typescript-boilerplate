"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
/**
 * @class UsersApi
 */
var UsersApi = (function () {
    function UsersApi() {
    }
    /**
     * Create a new user.
     * @param req {Request} The express request object.
     * @param res {Response} The express response object.
     * @param next {NextFunction} The next function to continue.
     */
    UsersApi.prototype.create = function (req, res, next) {
        // create user
        var user = new UsersApi.User(req.body);
        user.save().then(function (user) {
            res.json(user.toObject());
            next();
        }).catch(next);
    };
    /**
     * Delete a user.
     * @param req {Request} The express request object.
     * @param res {Response} The express response object.
     * @param next {NextFunction} The next function to continue.
     */
    UsersApi.prototype.delete = function (req, res, next) {
        // verify the id parameter exists
        var PARAM_ID = "id";
        if (req.params[PARAM_ID] === undefined) {
            res.sendStatus(404);
            next();
            return;
        }
        // get id
        var id = req.params[PARAM_ID];
        // get user
        UsersApi.User.findById(id).then(function (user) {
            // verify user exists
            if (user === null) {
                res.sendStatus(404);
                next();
                return;
            }
            user.remove().then(function () {
                res.sendStatus(200);
                next();
            }).catch(next);
        }).catch(next);
    };
    /**
     * Get a user.
     * @param req {Request} The express request object.
     * @param res {Response} The express response object.
     * @param next {NextFunction} The next function to continue.
     */
    UsersApi.prototype.get = function (req, res, next) {
        // verify the id parameter exists
        var PARAM_ID = "id";
        if (req.params[PARAM_ID] === undefined) {
            res.sendStatus(404);
            next();
            return;
        }
        // get id
        var id = req.params[PARAM_ID];
        // get user
        UsersApi.User.findById(id).then(function (user) {
            // verify user was found
            if (user === null) {
                res.sendStatus(404);
                next();
                return;
            }
            // send json of user object
            res.json(user.toObject());
            next();
        }).catch(next);
    };
    /**
     * List all users.
     * @param req {Request} The express request object.
     * @param res {Response} The express response object.
     * @param next {NextFunction} The next function to continue.
     */
    UsersApi.prototype.list = function (req, res, next) {
        // get users
        UsersApi.User.find().then(function (users) {
            res.json(users.map(function (user) { return user.toObject(); }));
            next();
        }).catch(next);
    };
    /**
     * Update a user.
     * @param req {Request} The express request object.
     * @param res {Response} The express response object.
     * @param next {NextFunction} The next function to continue.
     */
    UsersApi.prototype.update = function (req, res, next) {
        var PARAM_ID = "id";
        // verify the id parameter exists
        if (req.params[PARAM_ID] === undefined) {
            res.sendStatus(404);
            next();
            return;
        }
        // get id
        var id = req.params[PARAM_ID];
        // get user
        UsersApi.User.findById(id).then(function (user) {
            // verify user was found
            if (user === null) {
                res.sendStatus(404);
                next();
                return;
            }
            // save user
            Object.assign(user, req.body).save().then(function (user) {
                res.json(user.toObject());
                next();
            }).catch(next);
        }).catch(next);
    };
    return UsersApi;
}());
exports.UsersApi = UsersApi;
