import * as bodyParser from 'body-parser';
import * as cookieParser from 'cookie-parser';
import * as express from 'express';
import * as logger from 'morgan';
import * as path from 'path';
import * as cors from 'cors';
import errorHandler = require('errorhandler');
import methodOverride = require('method-override');
import mongoose = require('mongoose')

// Routes
import { IndexRoute } from './routes/index'

// REST APIs
import { UserRoute } from "./routes/api/user";

/**
 * The server.
 *
 * @class Server
 */
export class Server {

    public app: express.Application;

    private router: express.Router;

    private connection: mongoose.Connection;

    /**
     * Bootstrap the application.
     *
     * @class Server
     * @method bootstrap
     * @static
     * @return {ng.auto.IInjectorService} Returns the newly created injector for this app.
     */
    public static bootstrap(): Server {
        return new Server();
    }

    /**
     * Constructor.
     *
     * @class Server
     * @constructor
     */
    constructor() {

        //create expressjs application
        this.app = express();

        this.router = express.Router();

        //configure application
        this.config();

        //add routes
        this.app.use('/', this.router);
        this.routes(this.router);

        //add api
        this.api(this.router);
    }

    /**
     * Create REST API routes
     *
     * @class Server
     * @method api
     */
    public api(router: express.Router) {
        let apiRouter: express.Router = express.Router();
        const corsOptions: cors.CorsOptions = {
            allowedHeaders: ['Origin', 'X-Requested-With', 'Content-Type', 'Accept', 'X-Access-Token'],
            credentials: true,
            methods: 'GET,HEAD,OPTIONS,PUT,PATCH,POST,DELETE',
            origin: 'http://localhost:4200',
            preflightContinue: false
        };
        apiRouter.use(cors(corsOptions));

        apiRouter.get('/', (req: express.Request, res: express.Response, next: express.NextFunction) => {
            res.json({ announcement: 'Welcome to the API.' });
            next();
        });

        UserRoute.create(apiRouter, this.connection);

        apiRouter.options('*', cors(corsOptions));
        router.use('/api', apiRouter);
    }

    /**
     * Configure application
     *
     * @class Server
     * @method config
     */
    public config() {
        const MONGODB_CONNECTION: string = 'mongodb://localhost:27017/db';

        //add static paths
        this.app.use(express.static(path.join(__dirname, 'public')));

        //configure pug
        this.app.set('views', path.join(__dirname, 'views'));
        this.app.set('view engine', 'pug');

        //use logger middlware
        this.app.use(logger('dev'));

        //use json form parser middlware
        this.app.use(bodyParser.json());

        //use query string parser middlware
        this.app.use(bodyParser.urlencoded({
            extended: true
        }));

        //use cookie parser middleware
        this.app.use(cookieParser('SECRET_GOES_HERE'));

        //use override middlware
        this.app.use(methodOverride());
        
        // Use q promises
        global.Promise = require('q').Promise;
        mongoose.Promise = global.Promise;

        // connect to mongoose
        this.connection = mongoose.createConnection(MONGODB_CONNECTION);

        //catch 404 and forward to error handler
        this.app.use(function(err: any, req: express.Request, res: express.Response, next: express.NextFunction) {
            err.status = 404;
            next(err);
        });

        //error handling
        this.app.use(errorHandler());
    }

    /**
     * Create router
     *
     * @class Server
     * @method api
     */
    public routes(router: express.Router) {
        IndexRoute.create(router);
    }
}