import { NextFunction, Request, Response, Router } from "express";
import { BaseRoute } from "../BaseRoute";
import { UsersApi } from "../../api/user"
import * as mongoose from "mongoose";

/**
 * / route
 *
 * @class User
 */
export class UserRoute extends BaseRoute {

    /**
     * Create the routes.
     *
     * @class IndexRoute
     * @method create
     * @static
     */
    public static create(router: Router, connection: mongoose.Connection) {

        //log
        console.log("[UserApi::create] Creating user api routes.");

        // DELETE
        router.delete("/users/:id([0-9a-f]{24})", (req: Request, res: Response, next: NextFunction) => {
            new UsersApi(connection).delete(req, res, next);
        });

        // GET
        router.get("/users", (req: Request, res: Response, next: NextFunction) => {
            new UsersApi(connection).list(req, res, next);
        });
        router.get("/users/:id([0-9a-f]{24})", (req: Request, res: Response, next: NextFunction) => {
            new UsersApi(connection).get(req, res, next);
        });

        // POST
        router.post("/users", (req: Request, res: Response, next: NextFunction) => {
            new UsersApi(connection).create(req, res, next);
        });

        // PUT
        router.put("/users/:id([0-9a-f]{24})", (req: Request, res: Response, next: NextFunction) => {
            new UsersApi(connection).update(req, res, next);
        });
    }

    /**
     * Constructor
     *
     * @class IndexRoute
     * @constructor
     */
    constructor() {
        super();
    }
}