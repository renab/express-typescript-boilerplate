"use strict";
var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
Object.defineProperty(exports, "__esModule", { value: true });
var BaseRoute_1 = require("../BaseRoute");
var user_1 = require("../../api/user");
/**
 * / route
 *
 * @class User
 */
var UserRoute = (function (_super) {
    __extends(UserRoute, _super);
    /**
     * Constructor
     *
     * @class IndexRoute
     * @constructor
     */
    function UserRoute() {
        return _super.call(this) || this;
    }
    /**
     * Create the routes.
     *
     * @class IndexRoute
     * @method create
     * @static
     */
    UserRoute.create = function (router) {
        //log
        console.log("[UserApi::create] Creating user api routes.");
        // DELETE
        router.delete("/users/:id([0-9a-f]{24})", function (req, res, next) {
            new user_1.UsersApi().delete(req, res, next);
        });
        // GET
        router.get("/users", function (req, res, next) {
            new user_1.UsersApi().list(req, res, next);
        });
        router.get("/users/:id([0-9a-f]{24})", function (req, res, next) {
            new user_1.UsersApi().get(req, res, next);
        });
        // POST
        router.post("/users/", function (req, res, next) {
            new user_1.UsersApi().create(req, res, next);
        });
        // PUT
        router.put("/users/:id([0-9a-f]{24})", function (req, res, next) {
            new user_1.UsersApi().update(req, res, next);
        });
    };
    return UserRoute;
}(BaseRoute_1.BaseRoute));
exports.UserRoute = UserRoute;
